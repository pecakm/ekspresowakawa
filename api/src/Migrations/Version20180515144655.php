<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180515144655 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE coffee_order (id INT AUTO_INCREMENT NOT NULL, customer_id_id INT NOT NULL, coffee_id_id INT NOT NULL, status_id_id INT NOT NULL, seller_id_id INT DEFAULT NULL, order_date DATETIME NOT NULL, INDEX IDX_9BE3854AB171EB6C (customer_id_id), INDEX IDX_9BE3854A118E8885 (coffee_id_id), INDEX IDX_9BE3854A881ECFA7 (status_id_id), INDEX IDX_9BE3854ADF4C85EA (seller_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE coffee_order ADD CONSTRAINT FK_9BE3854AB171EB6C FOREIGN KEY (customer_id_id) REFERENCES user_account (id)');
        $this->addSql('ALTER TABLE coffee_order ADD CONSTRAINT FK_9BE3854A118E8885 FOREIGN KEY (coffee_id_id) REFERENCES coffee (id)');
        $this->addSql('ALTER TABLE coffee_order ADD CONSTRAINT FK_9BE3854A881ECFA7 FOREIGN KEY (status_id_id) REFERENCES order_status (id)');
        $this->addSql('ALTER TABLE coffee_order ADD CONSTRAINT FK_9BE3854ADF4C85EA FOREIGN KEY (seller_id_id) REFERENCES user_account (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE coffee_order');
    }
}
