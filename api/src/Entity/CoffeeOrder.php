<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoffeeOrderRepository")
 */
class CoffeeOrder {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserAccount", inversedBy="coffeeOrders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Coffee")
     * @ORM\JoinColumn(nullable=false)
     */
    private $coffeeId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrderStatus")
     * @ORM\JoinColumn(nullable=false)
     */
    private $statusId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $orderDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserAccount", inversedBy="coffeeSells")
     */
    private $sellerId;

    public function getId() {
        return $this->id;
    }

    public function getCustomerId(): ?UserAccount {
        return $this->customer_id;
    }

    public function setCustomerId(?UserAccount $customer_id): self {
        $this->customer_id = $customer_id;

        return $this;
    }

    public function getCoffeeId(): ?Coffee {
        return $this->coffeeId;
    }

    public function setCoffeeId(?Coffee $coffeeId): self {
        $this->coffeeId = $coffeeId;

        return $this;
    }

    public function getStatusId(): ?OrderStatus {
        return $this->statusId;
    }

    public function setStatusId(?OrderStatus $statusId): self {
        $this->statusId = $statusId;

        return $this;
    }

    public function getOrderDate(): ?\DateTimeInterface {
        return $this->orderDate;
    }

    public function setOrderDate(\DateTimeInterface $orderDate): self {
        $this->orderDate = $orderDate;

        return $this;
    }

    public function getSellerId(): ?UserAccount {
        return $this->sellerId;
    }

    public function setSellerId(?UserAccount $sellerId): self {
        $this->sellerId = $sellerId;

        return $this;
    }
}
