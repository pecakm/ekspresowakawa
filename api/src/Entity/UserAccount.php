<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserAccountRepository")
 */
class UserAccount implements UserInterface, \Serializable {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRole", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $role_id;

    /**
     * @ORM\Column(type="string", length=191, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CoffeeOrder", mappedBy="customer_id")
     */
    private $coffeeOrders;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CoffeeOrder", mappedBy="sellerId")
     */
    private $coffeeSells;

    public function __construct()
    {
        $this->coffeeOrders = new ArrayCollection();
        $this->coffeeSells = new ArrayCollection();
    }

    public function getId() {
        return $this->id;
    }

    public function getRoleId(): ?UserRole {
        return $this->role_id;
    }

    public function setRoleId(?UserRole $role_id): self {
        $this->role_id = $role_id;

        return $this;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    // UserInterface methods
    public function getUsername() {
        return $this->email;
    }

    public function getSalt() {
        return null;
    }

    public function getRoles() {
        return array($this->role_id->getName());
    }

    public function eraseCredentials() {
    }

    // Serializable methods
    public function serialize() {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password
        ));
    }

    public function unserialize($serialized) {
        list (
            $this->id,
            $this->email,
            $this->password
        ) = unserialize($serialized, ["allowed_classes" => false]);
    }

    /**
     * @return Collection|CoffeeOrder[]
     */
    public function getCoffeeOrders(): Collection
    {
        return $this->coffeeOrders;
    }

    public function addCoffeeOrder(CoffeeOrder $coffeeOrder): self
    {
        if (!$this->coffeeOrders->contains($coffeeOrder)) {
            $this->coffeeOrders[] = $coffeeOrder;
            $coffeeOrder->setCustomerId($this);
        }

        return $this;
    }

    public function removeCoffeeOrder(CoffeeOrder $coffeeOrder): self
    {
        if ($this->coffeeOrders->contains($coffeeOrder)) {
            $this->coffeeOrders->removeElement($coffeeOrder);
            // set the owning side to null (unless already changed)
            if ($coffeeOrder->getCustomerId() === $this) {
                $coffeeOrder->setCustomerId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CoffeeOrder[]
     */
    public function getCoffeeSells(): Collection
    {
        return $this->coffeeSells;
    }

    public function addCoffeeSell(CoffeeOrder $coffeeSell): self
    {
        if (!$this->coffeeSells->contains($coffeeSell)) {
            $this->coffeeSells[] = $coffeeSell;
            $coffeeSell->setSellerId($this);
        }

        return $this;
    }

    public function removeCoffeeSell(CoffeeOrder $coffeeSell): self
    {
        if ($this->coffeeSells->contains($coffeeSell)) {
            $this->coffeeSells->removeElement($coffeeSell);
            // set the owning side to null (unless already changed)
            if ($coffeeSell->getSellerId() === $this) {
                $coffeeSell->setSellerId(null);
            }
        }

        return $this;
    }
}
