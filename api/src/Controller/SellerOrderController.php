<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\CoffeeOrder;
use App\Entity\OrderStatus;

class SellerOrderController extends Controller {
    /**
     * @Route("/order/active", name="active_order_list")
     */
    public function getActiveOrderList() {
        $orders = $this->getDoctrine()->getRepository(CoffeeOrder::class)->findBy([
            "sellerId" => null
        ]);

        if (!$orders) {
            throw $this->createNotFoundException("No order found");
        }

        $encoders = array(new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($orders, "json");
        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }

    /**
     * @Route("/order/take", name="take_order")
     */
    public function takeOrder() {
        $request = Request::createFromGlobals();
        $content = $request->getContent();
        $jsonArray = json_decode($content, true);
        $orderId = $jsonArray["orderId"];
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        if (isset($orderId)) {
            $order = $this->getDoctrine()
            ->getRepository(CoffeeOrder::class)
            ->find($orderId);

            $order->setSellerId($user);

            $entityManager->persist($order);
            $entityManager->flush();
        }

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize("OK", "json");

        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }

    /**
     * @Route("/seller/orders", name="seller_order_list")
     */
    public function getSellerOrderList() {
        $user = $this->getUser();

        $orders = $this->getDoctrine()->getRepository(CoffeeOrder::class)->findBy([
            "sellerId" => $user
        ]);

        if (!$orders) {
            throw $this->createNotFoundException("No order found");
        }

        $encoders = array(new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $serializer = new Serializer(array($normalizer), $encoders);
        $jsonContent = $serializer->serialize($orders, "json");
        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }

    /**
     * @Route("/order/deliver", name="deliver_order")
     */
    public function deliverOrder() {
        $request = Request::createFromGlobals();
        $content = $request->getContent();
        $jsonArray = json_decode($content, true);
        $orderId = $jsonArray["orderId"];

        $entityManager = $this->getDoctrine()->getManager();

        if (isset($orderId)) {
            $status = $this->getDoctrine()
            ->getRepository(OrderStatus::class)
            ->find(2);

            $order = $this->getDoctrine()
            ->getRepository(CoffeeOrder::class)
            ->find($orderId);

            $order->setStatusId($status);

            $entityManager->persist($order);
            $entityManager->flush();
        }

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize("OK", "json");

        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }
}
