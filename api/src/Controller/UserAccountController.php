<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\UserAccount;
use App\Entity\UserRole;

class UserAccountController extends Controller {
    /**
     * @Route("/customer/register", name="customer_register")
     */
    public function registerCustomer(UserPasswordEncoderInterface $encoder) {
        $request = Request::createFromGlobals();
        $content = $request->getContent();
        $jsonArray = json_decode($content, true);
        $email = $jsonArray["email"];
        $password = $jsonArray["password"];

        $entityManager = $this->getDoctrine()->getManager();

        if (isset($email) && isset($password)) {
            if (strlen($password) > 5 && strlen($password) < 33 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $userRole = $this->getDoctrine()
                ->getRepository(UserRole::class)
                ->find(3);

                $userAccount = new UserAccount();
                $userAccount->setEmail($email);
                $encodedPassword = $encoder->encodePassword($userAccount, $password);
                $userAccount->setPassword($encodedPassword);
                $userAccount->setRoleId($userRole);

                $entityManager->persist($userAccount);
                $entityManager->flush();
            }
        }

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize("OK", "json");

        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }

    /**
     * @Route("/seller/register", name="seller_register")
     */
    public function registerSeller(UserPasswordEncoderInterface $encoder) {
        $request = Request::createFromGlobals();
        $content = $request->getContent();
        $jsonArray = json_decode($content, true);
        $email = $jsonArray["email"];
        $password = $jsonArray["password"];

        $entityManager = $this->getDoctrine()->getManager();

        if (isset($email) && isset($password)) {
            if (strlen($password) > 5 && strlen($password) < 33 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $userRole = $this->getDoctrine()
                ->getRepository(UserRole::class)
                ->find(2);

                $userAccount = new UserAccount();
                $userAccount->setEmail($email);
                $encodedPassword = $encoder->encodePassword($userAccount, $password);
                $userAccount->setPassword($encodedPassword);
                $userAccount->setRoleId($userRole);

                $entityManager->persist($userAccount);
                $entityManager->flush();
            }
        }

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize("OK", "json");

        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }

    /**
     * @Route("/login", name="user_login")
     */
    public function loginUser(UserPasswordEncoderInterface $encoder) {
        $request = Request::createFromGlobals();
        $content = $request->getContent();
        $jsonArray = json_decode($content, true);
        $email = $jsonArray["email"];
        $password = $jsonArray["password"];

        if (isset($email) && isset($password)) {
            if (strlen($password) > 5 && strlen($password) < 33 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $userAccount = $this->getDoctrine()
                ->getRepository(UserAccount::class)
                ->findOneBy([
                    "email" => $email
                ]);

                if (!$userAccount || !($encoder->isPasswordValid($userAccount, $password))) {
                    throw $this->createNotFoundException(
                        "No user found"
                    );
                }
            }
        }

        $data = array(
            "email" => $userAccount->getEmail(),
            "login" => $userAccount->getRoleId()->getId()
        );

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $jsonContent = $serializer->serialize($data, "json");

        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }
}
