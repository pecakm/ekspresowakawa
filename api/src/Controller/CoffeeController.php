<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Coffee;

class CoffeeController extends Controller {
    /**
     * @Route("/coffee", name="coffee")
     */
    public function getCoffeeList() {
        $coffees = $this->getDoctrine()->getRepository(Coffee::class)->findAll();

        if (!$coffees) {
            throw $this->createNotFoundException("No product found");
        }

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($coffees, "json");
        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }
}
