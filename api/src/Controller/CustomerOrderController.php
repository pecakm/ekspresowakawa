<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\UserAccount;
use App\Entity\Coffee;
use App\Entity\OrderStatus;
use App\Entity\CoffeeOrder;

class CustomerOrderController extends Controller
{
    /**
     * @Route("/order/make", name="make_order")
     */
    public function makeOrder() {
        $request = Request::createFromGlobals();
        $content = $request->getContent();
        $jsonArray = json_decode($content, true);
        $coffeeId = $jsonArray["coffeeId"];
        $user = $this->getUser();

        $entityManager = $this->getDoctrine()->getManager();

        if (isset($coffeeId)) {
            $customerAccount = $this->getDoctrine()
            ->getRepository(UserAccount::class)
            ->find($user->getId());

            $coffee = $this->getDoctrine()
            ->getRepository(Coffee::class)
            ->find(intval($coffeeId));

            $status = $this->getDoctrine()
            ->getRepository(OrderStatus::class)
            ->find(1);

            $coffeeOrder = new CoffeeOrder();
            $coffeeOrder->setCustomerId($customerAccount);
            $coffeeOrder->setCoffeeId($coffee);
            $coffeeOrder->setStatusId($status);
            $coffeeOrder->setOrderDate(new \DateTime("now"));

            $entityManager->persist($coffeeOrder);
            $entityManager->flush();
        }

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize("OK", "json");

        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }

    /**
     * @Route("/customer/orders", name="customer_order_list")
     */
    public function getCustomerOrderList() {
        $user = $this->getUser();

        $orders = $this->getDoctrine()->getRepository(CoffeeOrder::class)->findBy([
            "customer_id" => $user
        ]);

        if (!$orders) {
            throw $this->createNotFoundException("No order found");
        }

        $encoders = array(new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $serializer = new Serializer(array($normalizer), $encoders);
        $jsonContent = $serializer->serialize($orders, "json");
        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }
}
