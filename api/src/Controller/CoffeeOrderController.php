<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\CoffeeOrder;

class CoffeeOrderController extends Controller {
    /**
     * @Route("/order/all", name="order_all")
     */
    public function getOrderAll() {
        $orders = $this->getDoctrine()->getRepository(CoffeeOrder::class)->findAll();

        if (!$orders) {
            throw $this->createNotFoundException("No order found");
        }

        $encoders = array(new JsonEncoder());
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($orders, "json");
        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }
}
