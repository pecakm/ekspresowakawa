<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\CoffeeOrder;

class AdminListController extends Controller
{
    /**
     * @Route("/order/delete", name="delete_order")
     */
    public function deleteOrder() {
        $request = Request::createFromGlobals();
        $content = $request->getContent();
        $jsonArray = json_decode($content, true);
        $orderId = $jsonArray["orderId"];

        $entityManager = $this->getDoctrine()->getManager();

        if (isset($orderId)) {
            $order = $this->getDoctrine()
            ->getRepository(CoffeeOrder::class)
            ->find($orderId);

            $entityManager->remove($order);
            $entityManager->flush();
        }

        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize("OK", "json");

        $response = new JsonResponse();
        $response->setContent($jsonContent);

        return $response;
    }
}
