import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import '../css/Header.css';
import logo from '../img/logo.png';
import { setLoginSuccess, setToken } from '../actions/appActions';
import { setType } from '../actions/contentActions';
import { clearAdminList } from '../actions/admin/adminListActions';
import { clearCustomerOrders } from '../actions/customer/customerOrdersActions';
import { clearActiveOrders } from '../actions/seller/activeOrdersActions';
import { clearSellerOrders } from '../actions/seller/sellerOrdersActions';

class Header extends Component {
    setType() {
        this.props.setType("list");
    }

    logout() {
        this.props.setLoginSuccess(0);
        this.props.setToken("");
        this.props.clearAdminList();
        this.props.clearCustomerOrders();
        this.props.clearActiveOrders();
        this.props.clearSellerOrders();
    }

    render() {
        if (this.props.app.loginSuccess === 1) {
            return (
                <nav className="navbar navbar-expand-lg">
                    <Link className="navbar-brand" to="/ek">
                        <img onClick={() => this.setType()} className="img-fluid" src={logo} alt="logo"/>
                    </Link>
                    <div className="navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link onClick={() => this.logout()} className="nav-link" to="/ek">Wyloguj</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
            );
        } else if (this.props.app.loginSuccess === 2 || this.props.app.loginSuccess === 3) {
            return (
                <nav className="navbar navbar-expand-lg">
                    <Link className="navbar-brand" to="/ek">
                        <img onClick={() => this.setType()} className="img-fluid" src={logo} alt="logo"/>
                    </Link>
                    <div className="navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/ek/my-orders">Moje zamówienia</Link>
                            </li>
                            <li className="nav-item">
                                <Link onClick={() => this.logout()} className="nav-link" to="/ek">Wyloguj</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
            );
        } else {
            return (
                <nav className="navbar navbar-expand-lg">
                    <Link className="navbar-brand" to="/ek">
                        <img onClick={() => this.setType()} className="img-fluid" src={logo} alt="logo"/>
                    </Link>
                    <div className="navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/ek/login">Zaloguj</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.appReducer,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setLoginSuccess: (value) => {
            dispatch(setLoginSuccess(value));
        },
        setToken: (token) => {
            dispatch(setToken(token));
        },
        setType: (type) => {
            dispatch(setType(type));
        },
        clearAdminList: () => {
            dispatch(clearAdminList());
        },
        clearCustomerOrders: () => {
            dispatch(clearCustomerOrders());
        },
        clearActiveOrders: () => {
            dispatch(clearActiveOrders());
        },
        clearSellerOrders: () => {
            dispatch(clearSellerOrders());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);