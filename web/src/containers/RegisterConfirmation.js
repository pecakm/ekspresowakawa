import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { setRegisterSuccess } from '../actions/customer/customerFormActions';

class RegisterConfirmation extends Component {
    constructor(props) {
        super(props);
        this.props.setRegisterSuccess(false);
    }

    render() {
        return (
            <div className="content">
                <div className="interactive narrowDiv confirmation">
                    <p>Rejestracja zakończona!</p>
                    <Link to="/ek/login"><button className="btn">Zaloguj się</button></Link>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setRegisterSuccess: (type) => {
            dispatch(setRegisterSuccess(type));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterConfirmation);