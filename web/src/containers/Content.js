import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../css/Content.css';
import { CoffeeList } from '../components/customer/CoffeeList';
import ActiveOrders from './seller/ActiveOrders';
import AdminList from './admin/AdminList';
import CoffeeDetails from './customer/CoffeeDetails';
import { setType, setId, setList, setAddress } from '../actions/contentActions';
import { setAddressError } from '../actions/errorActions';

class Content extends Component {
    constructor(props) {
        super(props);
        this.changeAddressState = this.changeAddressState.bind(this);
        this.props.setAddressError("");
    }

    changeAddressState({target}) {
        this.props.setAddress(target.value);
        this.props.setAddressError("");
    }

    findAvailableCoffees() {
        if (this.props.content.address === "") {
            this.props.setAddressError("Wpisz adres dostawy!");
        } else {
            this.props.setType("list");
            this.props.setList(this.props.content.address);
        }
    }

    showPlaceholder() {
        if (this.props.content.address === "") {
            return "Podaj swój adres, np. Poznań, Grunwaldzka 138/1";
        } else {
            return this.props.content.address;
        }
    }

    showContent() {
        if (this.props.content.type === "list") {
            return (
                <CoffeeList address={this.props.content.address} data={this.props.content.data}/>
            );
        } else {
            return (
                <CoffeeDetails data={this.props.content.data[this.props.content.id]} dataId={this.props.content.id}/>
            );
        }
    }

    showAddressError() {
        return (
            <p className="error"><small>{this.props.error.addressError}</small></p>
        );
    }

    render() {
        if (this.props.app.loginSuccess === 1) {
            return (
                <AdminList/>
            );
        } else if (this.props.app.loginSuccess === 2) {
            return (
                <ActiveOrders/>
            );
        } else {
            return (
                <div className="content">
                    <div className="addressField interactive narrowDiv">
                        <input onChange={this.changeAddressState} className="form-control" type="text" placeholder={this.showPlaceholder()}/>
                        {this.showAddressError()}
                        <button className="btn" onClick={() => this.findAvailableCoffees()}>Wyszukaj dostępne kawy</button>
                    </div>
                    {this.showContent()}
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.appReducer,
        content: state.contentReducer,
        error: state.errorReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setType: (type) => {
            dispatch(setType(type));
        },
        setId: (id) => {
            dispatch(setId(id));
        },
        setAddress: (address) => {
            dispatch(setAddress(address));
        },
        setList: (address) => {
            dispatch(setList(address));
        },
        setAddressError: (message) => {
            dispatch(setAddressError(message));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Content);