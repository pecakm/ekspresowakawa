import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import { setTakeOrderSuccess } from '../../actions/errorActions';

class SellerOrderSummary extends Component {
    makeOrder() {
        let orderId = Number(this.props.orders.data[this.props.match.params.id].id);
        var setTakeOrderSuccess = this.props.setTakeOrderSuccess;
        axios({
            method: "post",
            url: "http://itpulse.pl/ek/api/public/order/take",
            data: {
                orderId: orderId
            },
            headers: {
                'X-AUTH-TOKEN': this.props.app.token
            }
        }).then(function (response) {
            if (response.data === "OK") {
                setTakeOrderSuccess("Przyjęto zamówienie");
            }
        }).catch(function (error) {
            console.log(error);
        });
    }

    render() {
        if (this.props.error.takeOrderSuccess === "") {
            let id = this.props.match.params.id;
            return (
                <div className="content">
                    <div className="goBackDiv">
                        <Link to="/ek"><button className="btn">Wróć</button></Link>
                    </div>
                    <div className="interactive narrowDiv confirmation">
                        <p><strong>Potwierdź</strong> przyjęcie zamówienia <span className="importantText">{this.props.orders.data[id].coffeeId.name}</span> dla <span className="importantText">{this.props.orders.data[id].customerId.email}</span></p>
                        <button onClick={() => this.makeOrder()} className="btn">Potwierdź</button>
                    </div>
                </div>
            );
        } else {
            return (
                <Redirect to="/ek/my-orders"/>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.appReducer,
        orders: state.activeOrdersReducer,
        error: state.errorReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setTakeOrderSuccess: (message) => {
            dispatch(setTakeOrderSuccess(message));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SellerOrderSummary);