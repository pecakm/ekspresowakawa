import React, { Component } from 'react';
import { connect } from 'react-redux';
import Spinner from 'react-spinkit';
import { SellerOrdersElement } from '../../components/seller/SellerOrdersElement';
import { getSellerOrders } from '../../actions/seller/sellerOrdersActions';
import { setTakeOrderSuccess, setChangeStatusSuccess } from '../../actions/errorActions';
import { setSpinnerVisibility } from '../../actions/spinnerActions';

class SellerOrders extends Component {
    constructor(props) {
        super(props);
        this.props.getSellerOrders(this.props.app.token);
        this.props.setTakeOrderSuccess("");
        this.props.setChangeStatusSuccess("");
        this.props.setSpinnerVisibility(true);
    }

    showEmptyDiv() {
        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Moje zamówienia</h2>
                <p>Nie masz jeszcze żadnych zrealizowanych zamówień</p>
            </div>
        );
    }

    showList() {
        let listItems = [];
        for (var i = this.props.list.data.length - 1; i >= 0; i--) {
            listItems.push(
                <SellerOrdersElement data={this.props.list.data[i]} id={i} key={i}/>
            );
        }

        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Moje zamówienia</h2>
                {listItems}
            </div>
        );
    }

    showSpinner() {
        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Moje zamówienia</h2>
                <Spinner name="circle"/>
            </div>
        );
    }

    render() {
        if (this.props.spinner.visible) {
            return this.showSpinner();
        } else {
            if (this.props.list.data.length === 0) {
                return this.showEmptyDiv();
            } else {
                return this.showList();
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        list: state.sellerOrdersReducer,
        app: state.appReducer,
        spinner: state.spinnerReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getSellerOrders: (token) => {
            dispatch(getSellerOrders(token));
        },
        setTakeOrderSuccess: (message) => {
            dispatch(setTakeOrderSuccess(message));
        },
        setChangeStatusSuccess: (message) => {
            dispatch(setChangeStatusSuccess(message));
        },
        setSpinnerVisibility: (type) => {
            dispatch(setSpinnerVisibility(type));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SellerOrders);