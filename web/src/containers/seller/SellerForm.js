import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import { setSellerEmail, setSellerPassword, setSellerPassword2 } from '../../actions/seller/sellerFormActions';
import { setRegisterSuccess } from '../../actions/customer/customerFormActions';
import { setSellerEmailError, setSellerPasswordError, setSellerPassword2Error, setSellerUserError } from '../../actions/errorActions';

class SellerForm extends Component {
    constructor(props) {
        super(props);
        this.changeEmailState = this.changeEmailState.bind(this);
        this.changePasswordState = this.changePasswordState.bind(this);
        this.changePassword2State = this.changePassword2State.bind(this);
        this.props.setSellerEmailError("");
        this.props.setSellerPasswordError("");
        this.props.setSellerPassword2Error("");
        this.props.setSellerUserError("");
    }

    changeEmailState({target}) {
        this.props.setSellerEmail(target.value);
        this.props.setSellerEmailError("");
    }

    changePasswordState({target}) {
        this.props.setSellerPassword(target.value);
        this.props.setSellerPasswordError("");
    }

    changePassword2State({target}) {
        this.props.setSellerPassword2(target.value);
        this.props.setSellerPassword2Error("");
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    register(email, password, password2) {
        if (!(this.validateEmail(email)) || email.length > 100) {
            this.props.setSellerEmailError("Podaj poprawny e-mail");
        } else if (password.length < 6 || password.length > 32) {
            this.props.setSellerPasswordError("Hasło musi mieć 6-32 znaków!");
        } else if (password !== password2) {
            this.props.setSellerPassword2Error("Hasła muszą być takie same!");
        } else {
            var setRegisterSuccess = this.props.setRegisterSuccess;
            var setSellerUserError = this.props.setSellerUserError;
            axios({
                method: "post",
                url: "http://itpulse.pl/ek/api/public/seller/register",
                data: {
                    email: email,
                    password: password
                },
                headers: {
                }
            }).then(function (response) {
                if (response.data === "OK") {
                    setRegisterSuccess(true);
                }
            }).catch(function (error) {
                setSellerUserError("Nie można zarejestrować użytkownika. Spróbuj użyć innego adresu e-mail.");
            });
        }
    }

    showEmailError() {
        if (this.props.error.sellerEmailError === "") {
            return null;
        } else {
            return (
                <small className="error">
                    {this.props.error.sellerEmailError}
                </small>
            );
        }
    }

    showPasswordError() {
        if (this.props.error.sellerPasswordError === "") {
            return null;
        } else {
            return (
                <small className="error">
                    {this.props.error.sellerPasswordError}
                </small>
            );
        }
    }

    showPassword2Error() {
        if (this.props.error.sellerPassword2Error === "") {
            return null;
        } else {
            return (
                <small className="error">
                    {this.props.error.sellerPassword2Error}
                </small>
            );
        }
    }

    showUserError() {
        if (this.props.error.sellerUserError === "") {
            return null;
        } else {
            return (
                <small className="error">
                    {this.props.error.sellerUserError}
                </small>
            );
        }
    }

    render() {
        if (this.props.customer.success === false) {
            return (
                <div className="content">
                    <div className="goBackDiv">
                        <Link to="/ek"><button className="btn">Wróć</button></Link>
                    </div>
                    <div className="form">
                        <h2>Rejestracja sprzedawcy</h2>
                        <div className="form-group">
                            <input onChange={this.changeEmailState} type="email" className="form-control" aria-describedby="emailHelp" placeholder="E-mail"/>
                            {this.showEmailError()}
                        </div>
                        <div className="form-group">
                            <input onChange={this.changePasswordState} type="password" className="form-control" placeholder="Hasło"/>
                            {this.showPasswordError()}
                        </div>
                        <div className="form-group">
                            <input onChange={this.changePassword2State} type="password" className="form-control" placeholder="Powtórz hasło"/>
                            {this.showPassword2Error()}
                        </div>
                        <button onClick={(email, password, password2) => this.register(this.props.seller.email, this.props.seller.password, this.props.seller.password2)} className="btn">Zarejestruj</button><br/>
                        {this.showUserError()}
                    </div>
                </div>
            );
        } else {
            return (
                <Redirect to="/ek/register-confirmation"/>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        seller: state.sellerFormReducer,
        customer: state.customerFormReducer,
        error: state.errorReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setSellerEmail: (email) => {
            dispatch(setSellerEmail(email));
        },
        setSellerPassword: (password) => {
            dispatch(setSellerPassword(password));
        },
        setSellerPassword2: (password2) => {
            dispatch(setSellerPassword2(password2));
        },
        setRegisterSuccess: (type) => {
            dispatch(setRegisterSuccess(type));
        },
        setSellerEmailError: (message) => {
            dispatch(setSellerEmailError(message));
        },
        setSellerPasswordError: (message) => {
            dispatch(setSellerPasswordError(message));
        },
        setSellerPassword2Error: (message) => {
            dispatch(setSellerPassword2Error(message));
        },
        setSellerUserError: (message) => {
            dispatch(setSellerUserError(message));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SellerForm);