import React, { Component } from 'react';
import { connect } from 'react-redux';
import Spinner from 'react-spinkit';
import { ActiveOrdersElement } from '../../components/seller/ActiveOrdersElement';
import { getActiveOrders } from '../../actions/seller/activeOrdersActions';
import { setSpinnerVisibility } from '../../actions/spinnerActions';

class ActiveOrders extends Component {
    constructor(props) {
        super(props);
        this.props.getActiveOrders(this.props.app.token);
        this.props.setSpinnerVisibility(true);
    }

    showActiveOrders() {
        let listItems = [];
        for (var i = 0; i < this.props.list.data.length; i++) {
            listItems.push(
                <ActiveOrdersElement data={this.props.list.data[i]} id={i} key={i}/>
            );
        }

        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Aktywne zamówienia</h2>
                {listItems}
            </div>
        );
    }

    showEmptyDiv() {
        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Aktywne zamówienia</h2>
                W tej chwili nie ma aktywnych zamówień.
            </div>
        );
    }

    showSpinner() {
        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Aktywne zamówienia</h2>
                <Spinner name="circle"/>
            </div>
        );
    }

    render() {
        if (this.props.spinner.visible) {
            return this.showSpinner();
        } else {
            if (this.props.list.data.length > 0) {
                return this.showActiveOrders();
            } else {
                return this.showEmptyDiv();
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        list: state.activeOrdersReducer,
        app: state.appReducer,
        spinner: state.spinnerReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getActiveOrders: (token) => {
            dispatch(getActiveOrders(token));
        },
        setSpinnerVisibility: (type) => {
            dispatch(setSpinnerVisibility(type));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ActiveOrders);
