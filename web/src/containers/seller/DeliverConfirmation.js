import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import { setChangeStatusSuccess } from '../../actions/errorActions';

class DeliverConfirmation extends Component {
    changeStatus(orderId) {
        var setChangeStatusSuccess = this.props.setChangeStatusSuccess;
        axios({
            method: "post",
            url: "http://itpulse.pl/ek/api/public/order/deliver",
            data: {
                orderId: orderId
            },
            headers: {
                "X-AUTH-TOKEN": this.props.app.token
            }
        }).then(function (response) {
            if (response.data === "OK") {
                setChangeStatusSuccess("Zmieniono status zamówienia.");
            }
        }).catch(function (error) {
            console.log(error);
        });
    }

    render() {
        let data = this.props.orders.data[this.props.match.params.id];
        if (this.props.error.changeStatusSuccess !== "") {
            return (
                <Redirect to="/ek/my-orders"/>
            );
        } else {
            return (
                <div className="content">
                    <div className="goBackDiv">
                        <Link to="/ek/my-orders"><button className="btn">Wróć</button></Link>
                    </div>
                    <div className="interactive narrowDiv confirmation">
                        <p><strong>Potwierdź</strong> dostarczenie kawy <span className="importantText">{data.coffeeId.name}</span> dla <span className="importantText">{data.customerId.email}</span></p>
                        <button onClick={(orderId) => this.changeStatus(data.id)} className="btn">Potwierdź</button>
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.appReducer,
        orders: state.sellerOrdersReducer,
        error: state.errorReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setChangeStatusSuccess: (message) => {
            dispatch(setChangeStatusSuccess(message));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DeliverConfirmation);