import React, { Component } from 'react';
import { connect } from 'react-redux';
import Spinner from 'react-spinkit';
import { AdminListElement } from '../../components/admin/AdminListElement';
import { getAdminList } from '../../actions/admin/adminListActions';
import { setDeleteOrderSuccess } from '../../actions/errorActions';
import { setSpinnerVisibility } from '../../actions/spinnerActions';

class AdminList extends Component {
    constructor(props) {
        super(props);
        this.props.getAdminList(this.props.app.token);
        this.props.setDeleteOrderSuccess("");
        this.props.setSpinnerVisibility(true);
    }

    showAdminList() {
        let listItems = [];
        for (var i = this.props.admin.data.length - 1; i >= 0; i--) {
            listItems.push(
                <AdminListElement data={this.props.admin.data[i]} id={i} key={i}/>
            );
        }

        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Wszystkie zamówienia</h2>
                {listItems}
            </div>
        );
    }

    showEmptyDiv() {
        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Wszystkie zamówienia</h2>
                <p>Nie ma jeszcze żadnych zamówień w historii</p>
            </div>
        );
    }

    showSpinner() {
        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Wszystkie zamówienia</h2>
                <Spinner name="circle"/>
            </div>
        );
    }

    render() {
        if (this.props.spinner.visible) {
            return this.showSpinner();
        } else {
            if (this.props.admin.data.length > 0) {
                return this.showAdminList();
            } else {
                return this.showEmptyDiv();
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        admin: state.adminListReducer,
        app: state.appReducer,
        spinner: state.spinnerReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAdminList: (token) => {
            dispatch(getAdminList(token));
        },
        setDeleteOrderSuccess: (message) => {
            dispatch(setDeleteOrderSuccess(message));
        },
        setSpinnerVisibility: (type) => {
            dispatch(setSpinnerVisibility(type));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminList);