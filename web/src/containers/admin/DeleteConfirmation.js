import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import { setDeleteOrderSuccess } from '../../actions/errorActions';

class DeleteConfirmation extends Component {
    deleteOrder(orderId) {
        var setDeleteOrderSuccess = this.props.setDeleteOrderSuccess;
        axios({
            method: "post",
            url: "http://itpulse.pl/ek/api/public/order/delete",
            data: {
                orderId: orderId
            },
            headers: {
                "X-AUTH-TOKEN": this.props.app.token
            }
        }).then(function (response) {
            if (response.data === "OK") {
                setDeleteOrderSuccess("Usunięto zamówienie.");
            }
        }).catch(function (error) {
            console.log(error);
        });
    }

    render() {
        let data = this.props.admin.data[this.props.match.params.id];
        if (this.props.error.deleteOrderSuccess !== "") {
            return (
                <Redirect to="/ek"/>
            );
        } else {
            return (
                <div className="content">
                    <div className="goBackDiv">
                        <Link to="/ek"><button className="btn">Wróć</button></Link>
                    </div>
                    <div className="interactive narrowDiv confirmation">
                        <p><strong>Potwierdź</strong> usunięcie zamówienia <span className="importantText">{data.coffeeId.name}</span> dla <span className="importantText">{data.customerId.email}</span></p>
                        <button onClick={(orderId) => this.deleteOrder(data.id)} className="btn">Potwierdź</button>
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.appReducer,
        admin: state.adminListReducer,
        error: state.errorReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setDeleteOrderSuccess: (message) => {
            dispatch(setDeleteOrderSuccess(message));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DeleteConfirmation);