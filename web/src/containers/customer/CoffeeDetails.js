import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import '../../css/CoffeeDetails.css';
import { setType } from '../../actions/contentActions';

class CoffeeDetails extends Component {
    goBack() {
        this.props.setType("list");
    }

    render() {
        return (
            <div className="details">
                <div className="goBackDiv">
                    <button className="btn" onClick={() => this.goBack()}>Wróć</button>
                </div>
                <div className="coffeeDetails interactive">
                    <p className="importantText">{this.props.data.name}</p>
                    <p>{this.props.data.description}</p>
                    <p>Objętość: <strong>{this.props.data.volume}ml</strong></p>
                    <p>Cena: <span className="importantText">{this.props.data.price / 100}zł</span></p>
                    <Link to={"/ek/order/" + this.props.dataId}><button className="btn">Zamów</button></Link>
                </div>
                <div className="coffeeImage">
                    <img className="img-fluid" src={require("../../img/" + this.props.data.imageName)} alt={this.props.data.name}/>
                </div>
                <div style={{"clear": "both"}}></div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        content: state.contentReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setType: (type) => {
            dispatch(setType(type));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CoffeeDetails);