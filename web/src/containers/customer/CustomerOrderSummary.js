import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import { setLoginError, setOrderSuccess } from '../../actions/errorActions';

class CustomerOrderSummary extends Component {
    constructor(props) {
        super(props);
        this.props.setLoginError("");
    }

    makeOrder() {
        var setOrderSuccess = this.props.setOrderSuccess;
        if (this.props.app.token === "") {
            this.props.setLoginError("Musisz się zalogować!");
        } else {
            let coffeeId = Number(this.props.content.data[this.props.match.params.id].id);
            axios({
                method: "post",
                url: "http://itpulse.pl/ek/api/public/order/make",
                data: {
                    coffeeId: coffeeId
                },
                headers: {
                    "X-AUTH-TOKEN": this.props.app.token
                }
            }).then(function (response) {
                if (response.data === "OK") {
                    setOrderSuccess("Złożono zamówienie");
                }
            }).catch(function (error) {
                console.log(error);
            });
        }
    }

    showLoginError() {
        if (this.props.error.loginError === "") {
            return null;
        } else {
            return (
                <p className="error"><small>{this.props.error.loginError}</small></p>
            );
        }
    }

    render() {
        let id = this.props.match.params.id;
        let data = this.props.content.data[id];
        if (this.props.error.orderSuccess !== "") {
            return (
                <Redirect to="/ek/my-orders"/>
            );
        } else {
            return (
                <div className="content">
                    <div className="goBackDiv">
                        <Link to="/ek"><button className="btn">Wróć</button></Link>
                    </div>
                    <div className="interactive narrowDiv confirmation">
                        <p><strong>Potwierdź</strong> zamówienie kawy <span className="importantText">{data.name}</span></p>
                        <button onClick={() => this.makeOrder()} className="btn">Potwierdź</button>
                        {this.showLoginError()}
                    </div>
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.appReducer,
        content: state.contentReducer,
        error: state.errorReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setLoginError: (message) => {
            dispatch(setLoginError(message));
        },
        setOrderSuccess: (message) => {
            dispatch(setOrderSuccess(message));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerOrderSummary);