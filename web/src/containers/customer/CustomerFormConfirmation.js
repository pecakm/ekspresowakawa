import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { setCustomerRegisterSuccess } from '../../actions/customer/customerFormActions';

class CustomerFormConfirmation extends Component {
    constructor(props) {
        super(props);
        this.props.setCustomerRegisterSuccess(false);
    }

    render() {
        return (
            <div className="content">
                <div className="interactive narrowDiv confirmation">
                    <p>Rejestracja zakończona!</p>
                    <Link to="/ek/login"><button className="btn">Zaloguj się</button></Link>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setCustomerRegisterSuccess: (type) => {
            dispatch(setCustomerRegisterSuccess(type));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerFormConfirmation);