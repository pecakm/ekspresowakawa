import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import { setCustomerEmail, setCustomerPassword, setCustomerPassword2, setRegisterSuccess } from '../../actions/customer/customerFormActions';
import { setCustomerEmailError, setCustomerPasswordError, setCustomerPassword2Error, setCustomerUserError } from '../../actions/errorActions';

class CustomerForm extends Component {
    constructor(props) {
        super(props);
        this.changeEmailState = this.changeEmailState.bind(this);
        this.changePasswordState = this.changePasswordState.bind(this);
        this.changePassword2State = this.changePassword2State.bind(this);
        this.props.setCustomerEmailError("");
        this.props.setCustomerPasswordError("");
        this.props.setCustomerPassword2Error("");
        this.props.setCustomerUserError("");
    }

    changeEmailState({target}) {
        this.props.setCustomerEmail(target.value);
        this.props.setCustomerEmailError("");
    }

    changePasswordState({target}) {
        this.props.setCustomerPassword(target.value);
        this.props.setCustomerPasswordError("");
    }

    changePassword2State({target}) {
        this.props.setCustomerPassword2(target.value);
        this.props.setCustomerPassword2Error("");
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    register(email, password, password2) {
        if (!(this.validateEmail(email)) || email.length > 100) {
            this.props.setCustomerEmailError("Podaj poprawny e-mail");
        } else if (password.length < 6 || password.length > 32) {
            this.props.setCustomerPasswordError("Hasło musi mieć 6-32 znaków!");
        } else if (password !== password2) {
            this.props.setCustomerPassword2Error("Hasła muszą być takie same!");
        } else {
            var setRegisterSuccess = this.props.setRegisterSuccess;
            var setCustomerUserError = this.props.setCustomerUserError;
            axios({
                method: "post",
                url: "http://itpulse.pl/ek/api/public/customer/register",
                data: {
                    email: email,
                    password: password
                },
                headers: {
                }
            }).then(function (response) {
                if (response.data === "OK") {
                    setRegisterSuccess(true);
                }
            }).catch(function (error) {
                setCustomerUserError("Nie można zarejestrować użytkownika. Spróbuj użyć innego adresu e-mail.");
            });
        }
    }

    showEmailError() {
        if (this.props.error.customerEmailError === "") {
            return null;
        } else {
            return (
                <small className="error">
                    {this.props.error.customerEmailError}
                </small>
            );
        }
    }

    showPasswordError() {
        if (this.props.error.customerPasswordError === "") {
            return null;
        } else {
            return (
                <small className="error">
                    {this.props.error.customerPasswordError}
                </small>
            );
        }
    }

    showPassword2Error() {
        if (this.props.error.customerPassword2Error === "") {
            return null;
        } else {
            return (
                <small className="error">
                    {this.props.error.customerPassword2Error}
                </small>
            );
        }
    }

    showUserError() {
        if (this.props.error.customerUserError === "") {
            return null;
        } else {
            return (
                <small className="error">
                    {this.props.error.customerUserError}
                </small>
            );
        }
    }

    render() {
        if (this.props.customer.success === false) {
            return (
                <div className="content">
                    <div className="goBackDiv">
                        <Link to="/ek"><button className="btn">Wróć</button></Link>
                    </div>
                    <div className="form">
                        <h2>Rejestracja</h2>
                        <div className="form-group">
                            <input onChange={this.changeEmailState} type="email" className="form-control" aria-describedby="emailHelp" placeholder="E-mail"/>
                            {this.showEmailError()}
                        </div>
                        <div className="form-group">
                            <input onChange={this.changePasswordState} type="password" className="form-control" placeholder="Hasło"/>
                            {this.showPasswordError()}
                        </div>
                        <div className="form-group">
                            <input onChange={this.changePassword2State} type="password" className="form-control" placeholder="Powtórz hasło"/>
                            {this.showPassword2Error()}
                        </div>
                        <button onClick={(email, password, password2) => this.register(this.props.customer.email, this.props.customer.password, this.props.customer.password2)} className="btn">Zarejestruj</button>
                        <small className="form-text text-muted">Masz już konto? <Link to="/ek/login">Zaloguj się</Link></small>
                    </div>
                </div>
            );
        } else {
            return (
                <Redirect to="/ek/register-confirmation"/>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        customer: state.customerFormReducer,
        error: state.errorReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setCustomerEmail: (email) => {
            dispatch(setCustomerEmail(email));
        },
        setCustomerPassword: (password) => {
            dispatch(setCustomerPassword(password));
        },
        setCustomerPassword2: (password2) => {
            dispatch(setCustomerPassword2(password2));
        },
        setRegisterSuccess: (type) => {
            dispatch(setRegisterSuccess(type));
        },
        setCustomerEmailError: (message) => {
            dispatch(setCustomerEmailError(message));
        },
        setCustomerPasswordError: (message) => {
            dispatch(setCustomerPasswordError(message));
        },
        setCustomerPassword2Error: (message) => {
            dispatch(setCustomerPassword2Error(message));
        },
        setCustomerUserError: (message) => {
            dispatch(setCustomerUserError(message));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerForm);