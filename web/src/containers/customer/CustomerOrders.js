import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Spinner from 'react-spinkit';
import { CustomerOrdersElement } from '../../components/customer/CustomerOrdersElement';
import { getCustomerOrders } from '../../actions/customer/customerOrdersActions';
import { setOrderSuccess } from '../../actions/errorActions';
import { setSpinnerVisibility } from '../../actions/spinnerActions';

class CustomerOrders extends Component {
    constructor(props) {
        super(props);
        this.props.getCustomerOrders(this.props.app.token);
        this.props.setOrderSuccess("");
        this.props.setSpinnerVisibility(true);
    }

    showEmptyDiv() {
        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Moje zamówienia</h2>
                <p>Nie masz jeszcze żadnych zamówień</p>
            </div>  
        );
    }

    showList() {
        let listItems = [];
        for (var i = this.props.list.data.length - 1; i >= 0; i--) {
            listItems.push(
                <CustomerOrdersElement data={this.props.list.data[i]} id={i} key={i}/>
            );
        }

        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Moje zamówienia</h2>
                {listItems}
            </div>
        );
    }

    showSpinner() {
        return (
            <div className="content narrowDiv">
                <h2 className="contentTitle">Moje zamówienia</h2>
                <Spinner name="circle"/>
            </div>
        );
    }

    render() {
        if (this.props.app.token !== "") {
            if (this.props.spinner.visible) {
                return this.showSpinner();
            } else {
                if (this.props.list.data.length === 0) {
                    return this.showEmptyDiv();
                } else {
                    return this.showList();
                }
            }
        } else {
            return (
                <Redirect to="/ek"/>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        list: state.customerOrdersReducer,
        app: state.appReducer,
        spinner: state.spinnerReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCustomerOrders: (token) => {
            dispatch(getCustomerOrders(token));
        },
        setOrderSuccess: (message) => {
            dispatch(setOrderSuccess(message));
        },
        setSpinnerVisibility: (type) => {
            dispatch(setSpinnerVisibility(type));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerOrders);
