import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import '../../css/CoffeeElement.css';
import { setType, setId } from '../../actions/contentActions';

class CoffeeElement extends Component {
    showDetails(id) {
        this.props.setType("details");
        this.props.setId(id);
    }

    render() {
        return (
            <div className="coffee interactive">
                <p className="importantText">
                    {this.props.data.name}<br/>
                    ({this.props.data.volume}ml)
                </p>
                <img className="img-fluid" src={require("../../img/" + this.props.data.imageName)} alt={this.props.data.name}/>
                <p>
                    Cena: <span className="importantText">{this.props.data.price / 100}zł</span>
                </p>
                <p>
                    <button className="btn" onClick={(id) => this.showDetails(this.props.id)}>Pokaż więcej</button>
                </p>
                <p>
                    <Link to={"/ek/order/" + this.props.id}><button className="btn">Zamów</button></Link>
                </p>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setType: (type) => {
            dispatch(setType(type));
        },
        setId: (id) => {
            dispatch(setId(id));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CoffeeElement);