import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import { setEmail, setPassword } from '../actions/loginFormActions';
import { setToken, setLoginSuccess } from '../actions/appActions';
import { setNoUserError, setLoginEmailError, setLoginPasswordError } from '../actions/errorActions';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.changeEmailState = this.changeEmailState.bind(this);
        this.changePasswordState = this.changePasswordState.bind(this);
        this.props.setLoginEmailError("");
        this.props.setLoginPasswordError("");
        this.props.setNoUserError("");
    }

    changeEmailState({target}) {
        this.props.setEmail(target.value);
        this.props.setLoginEmailError("");
    }

    changePasswordState({target}) {
        this.props.setPassword(target.value);
        this.props.setLoginPasswordError("");
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    
    login(email, password) {
        if (!(this.validateEmail(email)) || email.length > 100) {
            this.props.setLoginEmailError("Podaj poprawny e-mail");
        } else if (password.length < 6 || password.length > 32) {
            this.props.setLoginPasswordError("Hasło musi mieć 6-32 znaków!");
        } else {
            var setToken = this.props.setToken;
            var setSuccess = this.props.setLoginSuccess;
            var setNoUserError = this.props.setNoUserError;
            axios({
                method: "post",
                url: "http://itpulse.pl/ek/api/public/login",
                data: {
                    email: email,
                    password: password
                },
                headers: {
                }
            }).then(function (response) {
                setToken(response.data.email);
                setSuccess(response.data.login);
                setNoUserError("");
            }).catch(function (error) {
                setNoUserError("Błędny e-mail lub hasło.");
            });
        }
    }

    showEmailError() {
        if (this.props.error.loginEmailError === "") {
            return null;
        } else {
            return (
                <small className="error">
                    {this.props.error.loginEmailError}
                </small>
            );
        }
    }

    showPasswordError() {
        if (this.props.error.loginPasswordError === "") {
            return null;
        } else {
            return (
                <small className="error">
                    {this.props.error.loginPasswordError}
                </small>
            );
        }
    }

    render() {
        if (this.props.app.loginSuccess === 0) {
            return (
                <div className="content">
                    <div className="goBackDiv">
                        <Link to="/ek"><button className="btn">Wróć</button></Link>
                    </div>
                    <div className="form">
                        <h2>Logowanie</h2>
                        <div className="form-group">
                            <input onChange={this.changeEmailState} type="email" className="form-control" aria-describedby="emailHelp" placeholder="E-mail"/>
                            {this.showEmailError()}
                        </div>
                        <div className="form-group">
                            <input onChange={this.changePasswordState} type="password" className="form-control" placeholder="Hasło"/>
                            {this.showPasswordError()}
                        </div>
                        <p className="error"><small>{this.props.error.noUserError}</small></p>
                        <button onClick={(email, password) => this.login(this.props.login.email, this.props.login.password)} className="btn">Zaloguj</button>
                        <small className="form-text text-muted">Nie masz konta? <Link to="/ek/register">Zarejestruj się</Link></small>
                    </div>
                </div>
            );
        } else {
            return (
                <Redirect to="/ek"/>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        login: state.loginFormReducer,
        app: state.appReducer,
        error: state.errorReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setEmail: (email) => {
            dispatch(setEmail(email));
        },
        setPassword: (password) => {
            dispatch(setPassword(password));
        },
        setLoginSuccess: (value) => {
            dispatch(setLoginSuccess(value));
        },
        setToken: (token) => {
            dispatch(setToken(token));
        },
        setNoUserError: (message) => {
            dispatch(setNoUserError(message));
        },
        setLoginEmailError: (message) => {
            dispatch(setLoginEmailError(message));
        },
        setLoginPasswordError: (message) => {
            dispatch(setLoginPasswordError(message));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);