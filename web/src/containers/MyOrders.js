import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import SellerOrders from './seller/SellerOrders';
import CustomerOrders from './customer/CustomerOrders';

class MyOrders extends Component {
    render() {
        if (this.props.app.loginSuccess === 2) {
            return (
                <div className="content">
                    <div className="goBackDiv">
                        <Link to="/"><button className="btn">Wróć</button></Link>
                    </div>
                    <div>
                        <SellerOrders/>
                    </div>
                </div>
            );
        } else if (this.props.app.loginSuccess === 3) {
            return (
                <div className="content">
                    <div className="goBackDiv">
                        <Link to="/ek"><button className="btn">Wróć</button></Link>
                    </div>
                    <div>
                        <CustomerOrders/>
                    </div>
                </div>
            );
        } else {
            return (
                <Redirect to="/ek"/>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        app: state.appReducer
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyOrders);