import axios from 'axios';

export function getActiveOrders(token) {
    return dispatch => {
        axios({
            method: "get",
            url: "http://itpulse.pl/ek/api/public/order/active",
            data: {},
            headers: {
                "X-AUTH-TOKEN": token
            }
        }).then(function (response) {
            dispatch({
                type: "GET_ACTIVE_ORDERS",
                payload: response.data
            });
            dispatch({
                type: "SET_SPINNER_VISIBILITY",
                payload: false
            });
        }).catch(function (error) {
            dispatch({
                type: "SET_SPINNER_VISIBILITY",
                payload: false
            });
        });
    }
}

export function clearActiveOrders() {
    return {
        type: "CLEAR_ACTIVE_ORDERS",
        payload: []
    };
}
