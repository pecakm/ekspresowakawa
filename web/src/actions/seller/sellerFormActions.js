export function setSellerEmail(email) {
    return {
        type: "SET_SELLER_EMAIL",
        payload: email
    };
}

export function setSellerPassword(password) {
    return {
        type: "SET_SELLER_PASSWORD",
        payload: password
    };
}

export function setSellerPassword2(password2) {
    return {
        type: "SET_SELLER_PASSWORD2",
        payload: password2
    };
}
