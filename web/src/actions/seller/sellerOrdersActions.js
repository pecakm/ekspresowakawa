import axios from 'axios';

export function getSellerOrders(token) {
    return dispatch => {
        axios({
            method: "get",
            url: "http://itpulse.pl/ek/api/public/seller/orders",
            data: {},
            headers: {
                "X-AUTH-TOKEN": token
            }
        }).then(function (response) {
            dispatch({
                type: "GET_SELLER_ORDERS",
                payload: response.data
            });
            dispatch({
                type: "SET_SPINNER_VISIBILITY",
                payload: false
            });
        }).catch(function (error) {
            dispatch({
                type: "SET_SPINNER_VISIBILITY",
                payload: false
            })
        });
    }
}

export function clearSellerOrders() {
    return {
        type: "CLEAR_SELLER_ORDERS",
        payload: []
    };
}