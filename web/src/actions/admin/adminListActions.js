import axios from 'axios';

export function getAdminList(token) {
    return dispatch => {
        axios({
            method: "get",
            url: "http://itpulse.pl/ek/api/public/order/all",
            data: {},
            headers: {
                "X-AUTH-TOKEN": token
            }
        }).then(function (response) {
            dispatch({
                type: "GET_ADMIN_LIST",
                payload: response.data
            });
            dispatch({
                type: "SET_SPINNER_VISIBILITY",
                payload: false
            });
        }).catch(function (error) {
            dispatch({
                type: "SET_SPINNER_VISIBILITY",
                payload: false
            });
        });
    }
}

export function clearAdminList() {
    return {
        type: "CLEAR_ADMIN_LIST",
        payload: []
    };
}
