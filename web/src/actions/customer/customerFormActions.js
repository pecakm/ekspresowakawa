export function setCustomerEmail(email) {
    return {
        type: "SET_CUSTOMER_EMAIL",
        payload: email
    };
}

export function setCustomerPassword(password) {
    return {
        type: "SET_CUSTOMER_PASSWORD",
        payload: password
    };
}

export function setCustomerPassword2(password2) {
    return {
        type: "SET_CUSTOMER_PASSWORD2",
        payload: password2
    };
}

export function setRegisterSuccess(type) {
    return {
        type: "SET_REGISTER_SUCCESS",
        payload: type
    }
}
