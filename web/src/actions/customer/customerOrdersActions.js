import axios from 'axios';

export function getCustomerOrders(token) {
    return dispatch => {
        axios({
            method: "get",
            url: "http://itpulse.pl/ek/api/public/customer/orders",
            data: {},
            headers: {
                "X-AUTH-TOKEN": token
            }
        }).then(function (response) {
            dispatch({
                type: "GET_CUSTOMER_ORDERS",
                payload: response.data
            });
            dispatch({
                type: "SET_SPINNER_VISIBILITY",
                payload: false
            });
        }).catch(function (error) {
            dispatch({
                type: "SET_SPINNER_VISIBILITY",
                payload: false
            });
        });
    }
}

export function clearCustomerOrders() {
    return {
        type: "CLEAR_CUSTOMER_ORDERS",
        payload: []
    };
}