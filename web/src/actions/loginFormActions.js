export function setEmail(email) {
    return {
        type: "SET_LOGIN_EMAIL",
        payload: email
    };
}

export function setPassword(password) {
    return {
        type: "SET_LOGIN_PASSWORD",
        payload: password
    };
}
