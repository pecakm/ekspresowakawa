export function setToken(token) {
    return {
        type: "SET_TOKEN",
        payload: token
    };
}

export function setLoginSuccess(value) {
    return {
        type: "SET_LOGIN_SUCCESS",
        payload: value
    };
}