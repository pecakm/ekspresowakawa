export function setSpinnerVisibility(type) {
    return {
        type: "SET_SPINNER_VISIBILITY",
        payload: type
    };
}
