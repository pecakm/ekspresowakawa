import axios from 'axios';

export function setType(type) {
    return {
        type: "SET_TYPE",
        payload: type
    };
}

export function setId(id) {
    return {
        type: "SET_ID",
        payload: id
    };
}

export function setAddress(address) {
    return {
        type: "SET_ADDRESS",
        payload: address
    };
}

export function setList(address) {
    return dispatch => {
        axios({
            method: "get",
            url: "http://itpulse.pl/ek/api/public/coffee",
            data: {},
            headers: {}
        }).then(function (response) {
            dispatch({
                type: "SET_LIST",
                payload: response.data
            });
        }).catch(function (error) {
            console.log(error);
        });
    }
}
