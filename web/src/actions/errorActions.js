export function setLoginError(message) {
    return {
        type: "SET_LOGIN_ERROR",
        payload: message
    };
}

export function setNoUserError(message) {
    return {
        type: "SET_NO_USER_ERROR",
        payload: message
    };
}

export function setOrderSuccess(message) {
    return {
        type: "SET_ORDER_SUCCESS",
        payload: message
    };
}

export function setTakeOrderSuccess(message) {
    return {
        type: "SET_TAKE_ORDER_SUCCESS",
        payload: message
    };
}

export function setChangeStatusSuccess(message) {
    return {
        type: "SET_CHANGE_STATUS_SUCCESS",
        payload: message
    }
}

export function setDeleteOrderSuccess(message) {
    return {
        type: "SET_DELETE_ORDER_SUCCESS",
        payload: message
    }
}

export function setCustomerEmailError(message) {
    return {
        type: "SET_CUSTOMER_EMAIL_ERROR",
        payload: message
    }
}

export function setCustomerPasswordError(message) {
    return {
        type: "SET_CUSTOMER_PASSWORD_ERROR",
        payload: message
    }
}

export function setCustomerPassword2Error(message) {
    return {
        type: "SET_CUSTOMER_PASSWORD2_ERROR",
        payload: message
    }
}

export function setCustomerUserError(message) {
    return {
        type: "SET_CUSTOMER_USER_ERROR",
        payload: message
    }
}

export function setSellerEmailError(message) {
    return {
        type: "SET_SELLER_EMAIL_ERROR",
        payload: message
    }
}

export function setSellerPasswordError(message) {
    return {
        type: "SET_SELLER_PASSWORD_ERROR",
        payload: message
    }
}

export function setSellerPassword2Error(message) {
    return {
        type: "SET_SELLER_PASSWORD2_ERROR",
        payload: message
    }
}

export function setSellerUserError(message) {
    return {
        type: "SET_SELLER_USER_ERROR",
        payload: message
    }
}

export function setLoginEmailError(message) {
    return {
        type: "SET_LOGIN_EMAIL_ERROR",
        payload: message
    }
}

export function setLoginPasswordError(message) {
    return {
        type: "SET_LOGIN_PASSWORD_ERROR",
        payload: message
    }
}

export function setAddressError(message) {
    return {
        type: "SET_ADDRESS_ERROR",
        payload: message
    }
}