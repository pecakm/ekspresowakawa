import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './css/App.css';
import Header from './containers/Header';
import Content from './containers/Content';
import LoginForm from './containers/LoginForm';
import CustomerForm from './containers/customer/CustomerForm';
import SellerForm from './containers/seller/SellerForm';
import CustomerOrderSummary from './containers/customer/CustomerOrderSummary';
import SellerOrderSummary from './containers/seller/SellerOrderSummary';
import MyOrders from './containers/MyOrders';
import DeliverConfirmation from './containers/seller/DeliverConfirmation';
import RegisterConfirmation from './containers/RegisterConfirmation';
import { Footer } from './components/Footer';
import { Regulations } from './components/Regulations';
import DeleteConfirmation from './containers/admin/DeleteConfirmation';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <Header/>
          <Route exact path="/ek" component={Content}/>
          <Route path="/ek/login" component={LoginForm}/>
          <Route path="/ek/register" component={CustomerForm}/>
          <Route path="/ek/seller/register" component={SellerForm}/>
          <Route exact path="/ek/order/:id" component={CustomerOrderSummary}/>
          <Route exact path="/ek/order/take/:id" component={SellerOrderSummary}/>
          <Route path="/ek/my-orders" component={MyOrders}/>
          <Route path="/ek/deliver/:id" component={DeliverConfirmation}/>
          <Route path="/ek/delete/:id" component={DeleteConfirmation}/>
          <Route path="/ek/register-confirmation" component={RegisterConfirmation}/>
          <Route path="/ek/regulations" component={Regulations}/>
          <Footer/>
        </div>
      </Router>
    );
  }
}

export default App;
