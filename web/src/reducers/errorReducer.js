const errorReducer = (state = {
    loginError: "",
    orderSuccess: "",
    noUserError: "",
    takeOrderSuccess: "",
    changeStatusSuccess: "",
    deleteOrderSuccess: "",
    customerEmailError: "",
    customerPasswordError: "",
    customerPassword2Error: "",
    customerUserError: "",
    sellerEmailError: "",
    sellerPasswordError: "",
    sellerPassword2Error: "",
    sellerUserError: "",
    loginEmailError: "",
    loginPasswordError: "",
    addressError: ""
}, action) => {
    switch (action.type) {
        case "SET_LOGIN_ERROR":
            state = {
                ...state,
                loginError: action.payload
            };
            break;
        case "SET_NO_USER_ERROR":
            state = {
                ...state,
                noUserError: action.payload
            };
            break;
        case "SET_ORDER_SUCCESS":
            state = {
                ...state,
                orderSuccess: action.payload
            };
            break;
        case "SET_TAKE_ORDER_SUCCESS":
            state = {
                ...state,
                takeOrderSuccess: action.payload
            };
            break;
        case "SET_CHANGE_STATUS_SUCCESS":
            state = {
                ...state,
                changeStatusSuccess: action.payload
            };
            break;
        case "SET_DELETE_ORDER_SUCCESS":
            state = {
                ...state,
                deleteOrderSuccess: action.payload
            };
            break;
        case "SET_CUSTOMER_EMAIL_ERROR":
            state = {
                ...state,
                customerEmailError: action.payload
            }
            break;
        case "SET_CUSTOMER_PASSWORD_ERROR":
            state = {
                ...state,
                customerPasswordError: action.payload
            }
            break;
        case "SET_CUSTOMER_PASSWORD2_ERROR":
            state = {
                ...state,
                customerPassword2Error: action.payload
            }
            break;
        case "SET_CUSTOMER_USER_ERROR":
            state = {
                ...state,
                customerPassword2Error: action.payload
            }
            break;
        case "SET_SELLER_EMAIL_ERROR":
            state = {
                ...state,
                sellerEmailError: action.payload
            }
            break;
        case "SET_SELLER_PASSWORD_ERROR":
            state = {
                ...state,
                sellerPasswordError: action.payload
            }
            break;
        case "SET_SELLER_PASSWORD2_ERROR":
            state = {
                ...state,
                sellerPassword2Error: action.payload
            }
            break;
        case "SET_SELLER_USER_ERROR":
            state = {
                ...state,
                sellerUserError: action.payload
            }
            break;
        case "SET_LOGIN_EMAIL_ERROR":
            state = {
                ...state,
                loginEmailError: action.payload
            }
            break;
        case "SET_LOGIN_PASSWORD_ERROR":
            state = {
                ...state,
                loginPasswordError: action.payload
            }
            break;
        case "SET_ADDRESS_ERROR":
            state = {
                ...state,
                addressError: action.payload
            }
            break;
        default:
            break;
    }
    return state;
};

export default errorReducer;