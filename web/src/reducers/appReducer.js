const appReducer = (state = {
    token: "",
    loginSuccess: 0
}, action) => {
    switch (action.type) {
        case "SET_TOKEN":
            state = {
                ...state,
                token: action.payload
            };
            break;
        case "SET_LOGIN_SUCCESS":
            state = {
                ...state,
                loginSuccess: action.payload
            };
            break;
        default:
            break;
    }
    return state;
};

export default appReducer;