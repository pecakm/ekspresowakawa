const sellerFormReducer = (state = {
    email: "",
    password: "",
    password2: "",
    success: false
}, action) => {
    switch (action.type) {
        case "SET_SELLER_EMAIL":
            state = {
                ...state,
                email: action.payload
            };
            break;
        case "SET_SELLER_PASSWORD":
            state = {
                ...state,
                password: action.payload
            };
            break;
        case "SET_SELLER_PASSWORD2":
            state = {
                ...state,
                password2: action.payload
            };
            break;
        default:
            break;
    }
    return state;
};

export default sellerFormReducer;