const sellerOrdersReducer = (state = {
    data: []
}, action) => {
    switch (action.type) {
        case "GET_SELLER_ORDERS":
            state = {
                ...state,
                data: action.payload
            };
            break;
        case "CLEAR_SELLER_ORDERS":
            state= {
                ...state,
                data: action.payload
            };
            break;
        default:
            break;
    }
    return state;
};

export default sellerOrdersReducer;