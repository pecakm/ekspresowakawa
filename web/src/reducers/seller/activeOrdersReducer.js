const activeOrdersReducer = (state = {
    data: []
}, action) => {
    switch (action.type) {
        case "GET_ACTIVE_ORDERS":
            state = {
                ...state,
                data: action.payload
            };
            break;
        case "CLEAR_ACTIVE_ORDERS":
            state= {
                ...state,
                data: action.payload
            };
            break;
        default:
            break;
    }
    return state;
};

export default activeOrdersReducer;
