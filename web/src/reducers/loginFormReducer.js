const loginFormReducer = (state = {
    email: "",
    password: ""
}, action) => {
    switch (action.type) {
        case "SET_LOGIN_EMAIL":
            state = {
                ...state,
                email: action.payload
            };
            break;
        case "SET_LOGIN_PASSWORD":
            state = {
                ...state,
                password: action.payload
            };
            break;
        default:
            break;
    }
    return state;
};

export default loginFormReducer;