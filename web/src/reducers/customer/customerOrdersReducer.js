const customerOrdersReducer = (state = {
    data: []
}, action) => {
    switch (action.type) {
        case "GET_CUSTOMER_ORDERS":
            state = {
                ...state,
                data: action.payload
            };
            break;
        case "CLEAR_CUSTOMER_ORDERS":
            state = {
                ...state,
                data: action.payload
            };
            break;
        default:
            break;
    }
    return state;
};

export default customerOrdersReducer;
