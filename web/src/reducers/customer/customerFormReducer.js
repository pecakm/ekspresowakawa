const customerFormReducer = (state = {
    email: "",
    password: "",
    password2: "",
    success: false
}, action) => {
    switch (action.type) {
        case "SET_CUSTOMER_EMAIL":
            state = {
                ...state,
                email: action.payload
            };
            break;
        case "SET_CUSTOMER_PASSWORD":
            state = {
                ...state,
                password: action.payload
            };
            break;
        case "SET_CUSTOMER_PASSWORD2":
            state = {
                ...state,
                password2: action.payload
            };
            break;
        case "SET_REGISTER_SUCCESS":
            state = {
                ...state,
                success: action.payload
            }
            break;
        default:
            break;
    }
    return state;
};

export default customerFormReducer;