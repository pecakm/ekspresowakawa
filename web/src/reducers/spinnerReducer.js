const spinnerReducer = (state = {
    visible: false
}, action) => {
    switch (action.type) {
        case "SET_SPINNER_VISIBILITY":
            state = {
                ...state,
                visible: action.payload
            };
            break;
        default:
            break;
    }
    return state;
};

export default spinnerReducer;