const adminListReducer = (state = {
    data: []
}, action) => {
    switch (action.type) {
        case "GET_ADMIN_LIST":
            state = {
                ...state,
                data: action.payload
            };
            break;
        case "CLEAR_ADMIN_LIST":
            state = {
                ...state,
                data: action.payload
            };
            break;
        default:
            break;
    }
    return state;
};

export default adminListReducer;