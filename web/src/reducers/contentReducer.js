const contentReducer = (state = {
    type: "list",
    id: 0,
    address: "",
    data: []
}, action) => {
    switch (action.type) {
        case "SET_TYPE":
            state = {
                ...state,
                type: action.payload
            };
            break;
        case "SET_ID":
            state = {
                ...state,
                id: action.payload
            };
            break;
        case "SET_ADDRESS":
            state = {
                ...state,
                address: action.payload
            };
            break;
        case "SET_LIST":
            state = {
                ...state,
                data: action.payload
            };
            break;
        default:
            break;
    }
    return state;
};

export default contentReducer;
