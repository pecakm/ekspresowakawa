import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/Regulations.css';

export class Regulations extends Component {
    render() {
        return (
            <div className="content">
                <div className="goBackDiv">
                    <Link to="/ek"><button className="btn">Wróć</button></Link>
                </div>
                <div className="regulations narrowDiv">
                    <h2 className="contentTitle">Regulamin</h2>
                    <p>1. Portal Ekspresowa Kawa został stworzony przez markę <a href="http://itpulse.pl" target="_blank" rel="noopener noreferrer">IT Pulse</a> i jest jej własnością.</p>
                    <p>2. Ten regulamin powstał tylko na potrzeby stworzenia dodatkowego widoku :)</p>
                </div>
            </div>
        );
    }
}
