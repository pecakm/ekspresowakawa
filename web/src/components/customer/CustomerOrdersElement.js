import React, { Component } from 'react';
import Moment from 'react-moment';

export class CustomerOrdersElement extends Component {
    render() {
        return (
            <div className="interactive listItem">
                <p className="importantText">{this.props.data.coffeeId.name}</p>
                Data zamówienia: <strong><Moment unix format="DD/MM/YYYY HH:mm">{this.props.data.orderDate.timestamp}</Moment></strong><br/>
                Status: <strong>{this.props.data.statusId.name}</strong>
            </div>
        );
    }
}
