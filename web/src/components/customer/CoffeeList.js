import React, { Component } from 'react';
import CoffeeElement from '../../containers/customer/CoffeeElement';

export class CoffeeList extends Component {
    showEmptyDiv() {
        return (
            <div/>
        );
    }

    showCoffeesList() {
        let menuItems = [];
        for (var i = 0; i < this.props.data.length; i++) {
            menuItems.push(
                <CoffeeElement data={this.props.data[i]} id={i} key={i}/>
            );
        }

        return (
            <div>{menuItems}</div>
        );
    }

    render() {
        if (this.props.data.length > 0) {
            return this.showCoffeesList();
        } else {
            return this.showEmptyDiv();
        }
    }
}
