import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import '../../css/ordersElement.css';

export class SellerOrdersElement extends Component {
    showDeliverButton() {
        if (this.props.data.statusId.id === 1) {
            return (
                <div className="orderButton">
                    <Link to={"/ek/deliver/" + this.props.id}><button className="btn">Dostarczono</button></Link>
                </div>
            );
        } else {
            return (
                <div/>
            );
        }
    }

    render() {
        return (
            <div className="interactive listItem">
                <div className="info">
                    <p className="importantText">{this.props.data.coffeeId.name}</p>
                    Zamawiający: <strong>{this.props.data.customerId.email}</strong><br/>
                    Data zamówienia: <strong><Moment unix format="DD/MM/YYYY HH:mm">{this.props.data.orderDate.timestamp}</Moment></strong><br/>
                    Status: <strong>{this.props.data.statusId.name}</strong>
                </div>
                {this.showDeliverButton()}
                <div style={{clear: "both"}}/>
            </div>
        );
    }
}
