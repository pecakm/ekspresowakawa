import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import '../../css/ordersElement.css';

export class ActiveOrdersElement extends Component {
    render() {
        return (
            <div className="interactive listItem">
                <div className="info">
                    <p className="importantText">{this.props.data.coffeeId.name}</p>
                    Zamawiający: <strong>{this.props.data.customerId.email}</strong><br/>
                    Data zamówienia: <strong><Moment unix format="DD/MM/YYYY HH:mm">{this.props.data.orderDate.timestamp}</Moment></strong>
                </div>
                <div className="orderButton">
                <Link to={"/ek/order/take/" + this.props.id}><button className="btn">Przyjmij zamówienie</button></Link>
                </div>
                <div style={{clear: "both"}}/>
            </div>
        );
    }
}
