import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';

export class AdminListElement extends Component {
    showDeleteButton() {
        if (this.props.data.sellerId == null) {
            return (
                <div className="orderButton">
                    <Link to={"/ek/delete/" + this.props.id}><button className="btn">Usuń zamówienie</button></Link>
                </div>
            );
        } else {
            return (
                <div/>
            );
        }
    }

    render() {
        let sellerEmail = "";
        if (this.props.data.sellerId == null) {
            sellerEmail = "brak";
        } else {
            sellerEmail = this.props.data.sellerId.email;
        }

        return (
            <div className="listItem interactive">
                <div className="info">
                    <p className="importantText">{this.props.data.coffeeId.name}</p>
                    Zamawiający: <strong>{this.props.data.customerId.email}</strong><br/>
                    Wykonujący: <strong>{sellerEmail}</strong><br/>
                    Data zamówienia: <strong><Moment unix format="DD/MM/YYYY HH:mm">{this.props.data.orderDate.timestamp}</Moment></strong><br/>
                    Status: <strong>{this.props.data.statusId.name}</strong>
                </div>
                {this.showDeleteButton()}
                <div style={{clear: "both"}}/>
            </div>
        );
    }
}
