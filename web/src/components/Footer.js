import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/Footer.css';

export class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <hr/>
                <p>
                    <Link to="/ek/regulations"><small>Regulamin</small></Link><br/>
                    <Link to="/ek/seller/register"><small>Współpraca</small></Link><br/>
                    <Link to=""><small>Kontakt</small></Link>
                </p>
                <p className="copyright">Copyright &copy; <a target="_blank" rel="noopener noreferrer" href="http://itpulse.pl">IT Pulse</a></p>
            </div>
        );
    }
}
