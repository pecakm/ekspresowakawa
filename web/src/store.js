import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import appReducer from './reducers/appReducer';
import contentReducer from './reducers/contentReducer';
import customerFormReducer from './reducers/customer/customerFormReducer';
import sellerFormReducer from './reducers/seller/sellerFormReducer';
import loginFormReducer from './reducers/loginFormReducer';
import activeOrdersReducer from './reducers/seller/activeOrdersReducer';
import adminListReducer from './reducers/admin/adminListReducer';
import sellerOrdersReducer from './reducers/seller/sellerOrdersReducer';
import customerOrdersReducer from './reducers/customer/customerOrdersReducer';
import errorReducer from './reducers/errorReducer';
import spinnerReducer from './reducers/spinnerReducer';
import { loadState } from './localStorage';

const persistedState = loadState();

export default createStore(
    combineReducers({
        appReducer,
        contentReducer,
        customerFormReducer,
        sellerFormReducer,
        loginFormReducer,
        activeOrdersReducer,
        adminListReducer,
        sellerOrdersReducer,
        customerOrdersReducer,
        errorReducer,
        spinnerReducer
    }),
    persistedState,
    applyMiddleware(thunk)
);
